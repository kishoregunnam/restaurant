import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ReservationService } from '@/_services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userData;

  userReservationsData;
  reservationsColumns = ['id', 'fromDate', 'timeSlot', 'guests', 'tableId', 'email', 'phone', 'status'];
  constructor(private user: AuthenticationService,private userReservations: ReservationService) {
      user.currentUser.subscribe(data => {
        this.userData = data;
      });
      userReservations.getUserResevations(this.userData.id).subscribe(data => {
        this.userReservationsData = data;
      });
   }

  ngOnInit() {
  }

}
