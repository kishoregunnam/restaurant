import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  questions: string [] = [
    'Overall Dining Experience *',
    'How was the food? *',
    'Service *',
    'How was the restaurant setting? *',
    'How would you rate us on Value for Money? *'
  ];
  options: any = {
    'very-bad': 'very Bad',
    'bad' : 'Bad',
    'satisfactory': 'Satisfactory',
    'good': 'Good',
    'excellent': 'Excellent'
  };
  constructor() {
    console.log(this.options);
  }

  ngOnInit() {
  }

}
