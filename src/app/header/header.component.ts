import { Component, OnInit } from '@angular/core';
import { Config, AuthenticationService } from '@/_services';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  appName: string;
  isLoggedIn: Observable<boolean>;
  isUser: Observable<boolean>;
  constructor(conf: Config, auth: AuthenticationService) {
    auth.is_logged_in();
    // this.appName = global.appName;
    this.appName = conf.appTitle;
    this.isLoggedIn = auth.loggedIn;
  }
}
