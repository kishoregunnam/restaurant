﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

import { User, Role, FoodCategory, Menu, Order } from '@/_models';
import { Table } from '@/_models/table';
import { Reservation } from '@/_models/reservation';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const USERS_DATA: User[] = [
            {
                id: 1,
                username: 'admin',
                password: 'admin',
                firstName: 'Admin',
                lastName: 'User',
                role: Role.Admin ,
                phone: '9999999999',
                email: 'admin@admin.com',
                gender: 'male'
            },
            {
                id: 2,
                username: 'user',
                password: 'user',
                firstName: 'Normal',
                lastName: 'User',
                role: Role.User ,
                phone: '9999999989',
                email: 'user@user.com',
                gender: 'female'
            },
            {
                id: 3,
                username: 'kishore',
                password: 'kishore',
                firstName: 'kishore',
                lastName: 'Gunnam',
                role: Role.User,
                phone: '9032551622',
                email: 'kishore@worker.com',
                gender: 'male'
            },
            {
                id: 4,
                username: 'manager',
                password: 'manager',
                firstName: 'manager',
                lastName: 'manager',
                role: Role.Manager,
                phone: '9988774455',
                email: 'manager@manager.com',
                gender: 'male'
            },
            {
                id: 5,
                username: 'manager1',
                password: 'manager1',
                firstName: 'manager1',
                lastName: 'manager1',
                role: Role.Manager,
                phone: '9988784455',
                email: 'manager@manager1.com',
                gender: 'female'
            },
            {
                id: 6,
                username: 'chef',
                password: 'chef',
                firstName: 'chef',
                lastName: 'chef',
                role: Role.Chef,
                phone: '9955584455',
                email: 'chef@chef.com',
                gender: 'male'
            }
        ];

        const CATEGORY_DATA:  FoodCategory[] = [
            {
                id: 1,
                name: 'BIRYANI'
            },
            {
                id: 2,
                name: 'KEBABS VEG'
            },
            {
                id: 3,
                name: 'KEBABS NON-VEG'
            },
            {
                id: 4,
                name: 'STARTERS'
            },
            {
                id: 5,
                name: 'MAINS VEG'
            },
            {
                id: 6,
                name: 'MAINS NON-VEG'
            },
            {
                id: 7,
                name: 'INDIAN BREADS'
            }
          ];

        const FOOD_MENU_DATA: Menu[] = [
            {
                id: 1,
                name: 'Chicken Biryani',
                description: 'Chicken and basmati rice cooked in layers,flavoured with saffron, served with raita and mirchi-ka-salan',
                category: 'BIRYANI',
                price: 289
            },
            {
                id: 2,
                name: 'Mutton Biryani',
                description: 'An aromatic mixture of rice, lamb, saffron, herbs and spcies, served with raita and mirchi-ka-salan',
                category: 'BIRYANI',
                price: 319
            },
            {
                id: 3,
                name: 'Special Biryani',
                description: 'A rare combination of chicken and lamb with saffron served with raita and mirchi-ka-salan',
                category: 'BIRYANI',
                price: 1034
            },
            {
                id: 4,
                name: 'Special Supreme Chicken Biryani',
                description: "A magnificent, extra large portion of the world's favourite Biryani.Great for sharing(serves 4-5 guests)",
                category: 'BIRYANI',
                price: 1099
            },
            {
                id: 5,
                name: 'Paneer Achari',
                description: 'ATandoor cooked paneer in pickle flavoured marinade',
                category: 'KEBABS VEG',
                price: 251
            },
            {
                id: 6,
                name: 'Paneer Tikka Kebab',
                description: 'Succulent pieces of paneer marinated with spices and chillies, grilled in the tandoor',
                category: 'KEBABS VEG',
                price: 251
            },
            {
                id: 7,
                name: 'Chicken Tikka Kebab',
                description: 'Chef’s Special - boneless pieces of chicken marinated in secret spices, grilled in the tandoor',
                category: 'KEBABS NON-VEG',
                price: 319
            },
            {
                id: 8,
                name: 'Chicken Garlic Kebab',
                description: 'Boneless pieces of chicken marinated in mild garlic paste, grilled in the tandoor',
                category: 'KEBABS NON-VEG',
                price: 319
            },
            {
                id: 9,
                name: 'Veg. Manchurian',
                description: 'Stir fried vegetable dumplings in spiced Manchurian sauce',
                category: 'STARTERS',
                price: 253
            },
            {
                id: 10,
                name: 'Chilli Paneer',
                description: 'Wok tossed cubes of deep fried paneer,coated with tangy chilli sauce',
                category: 'STARTERS',
                price: 262
            },
            {
                id: 11,
                name: 'Chicken 65 - NON VEG',
                description: 'Tender chicken pakoras tossed in spiced yoghurt sauce with curry',
                category: 'STARTERS',
                price: 354
            },
            {
                id: 12,
                name: 'Chilli Chicken- NON VEG',
                description: 'Wok tossed cubes of deep fried chicken, coated with tangy chilli sauce',
                category: 'STARTERS',
                price: 354
            },
            {
                id: 13,
                name: 'Dal Fry',
                description: 'A rich blend of split red and yellow gram,garnished with coriander leaves and butter',
                category: 'MAINS VEG',
                price: 229
            },
            {
                id: 14,
                name: 'Dal Makhani',
                description: 'Simmered black gram and red kidney beans finished with butter and cream',
                category: 'MAINS VEG',
                price: 229
            },
            {
                id: 15,
                name: 'Kadai Veg.',
                description: 'Seasonal vegetables cooked in a spicy gravy',
                category: 'MAINS VEG',
                price: 229
            },
            {
                id: 16,
                name: 'Butter Chicken Boneless',
                description: 'SChicken marinated in yoghurt and spices, cooked in tomato puree and cream',
                category: 'MAINS NON-VEG',
                price: 330
            },
            {
                id: 17,
                name: 'Chicken Tikka Masala',
                description: 'Chicken tikka cooked in rich fenugreek flavoured tomato gravy',
                category: 'MAINS NON-VEG',
                price: 330
            },
            {
                id: 18,
                name: 'Mutton Masala',
                description: 'Lamb (with bones) cooked in rich aromatic gravy',
                category: 'MAINS NON-VEG',
                price: 354
            },
            {
                id: 19,
                name: 'Tandoori Roti',
                description: '',
                category: 'INDIAN BREADS',
                price: 52
            },
            {
                id: 20,
                name: 'Butter Roti',
                description: '',
                category: 'INDIAN BREADS',
                price: 67
            },
            {
                id: 21,
                name: 'Plain Naan',
                description: '',
                category: 'INDIAN BREADS',
                price: 67
            },
            {
                id: 22,
                name: 'Butter Naan',
                description: '',
                category: 'INDIAN BREADS',
                price: 77
            }
        ];

        const TABLE_DATA: Table[] = [
            {
                tableId: 1,
                tableCapacity: 10,
                minimumCapacity: 8,
                manager: 4
            },
            {
                tableId: 2,
                tableCapacity: 4,
                minimumCapacity: 3,
                manager: 4
            },
            {
                tableId: 3,
                tableCapacity: 2,
                minimumCapacity: 1,
                manager: 5
            },
            {
                tableId: 4,
                tableCapacity: 10,
                minimumCapacity: 8,
                manager: 5
            }
        ];


        const RESERVATIONS_DATA: Reservation[] = [
            {
                id: 1,
                userId: 2,
                fromDate: '12-04-2018',
                guests: 4,
                tableId: 1,
                timeSlot: '15:30',
                firstname: 'hari',
                lastname: 'barma',
                email: 'kishore@gmail.com',
                phone: 9874504152,
                address: 'd-no:1-120,hyderbad',
                note: 'special',
                created_at: '12-04-2018T11:30',
                status: 'confirmed'
            },
            {
                id: 2,
                userId: 4,
                fromDate: '13-04-2018',
                guests: 3,
                tableId: 3,
                timeSlot: '15:00',
                firstname: 'kishore',
                lastname: 'varma',
                email: 'kishore12@gmail.com',
                phone: 9874504741,
                address: 'd-no:1-180,hyderbad',
                note: 'special',
                created_at: '11-04-2018T11:30',
                status: 'pending'
            }
        ];

        const ORDERS_DATA: Order[] = [];

        const TIME_SLOTS = ['12:00 PM', '2:00 PM', '6:00 PM', '8:00 PM', '9:30 PM', '10:00 PM'];

        const authHeader = request.headers.get('Authorization');
        const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');
        const roleString = isLoggedIn && authHeader.split('.')[1];
        const role = roleString ? Role[roleString] : null;

        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate - public
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                const user = USERS_DATA.find(x => x.username === request.body.username && x.password === request.body.password);
                if (!user) { return error('Username or password is incorrect'); }
                return ok({
                    id: user.id,
                    username: user.username,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    phone: user.phone,
                    role: user.role,
                    gender: user.gender,
                    token: `fake-jwt-token.${user.role}`
                });
            }

            // get user by id - admin or user (user can only access their own record)
            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
                if (!isLoggedIn) { return unauthorised(); }

                // get id from request url
                const urlParts = request.url.split('/');
                const id = parseInt(urlParts[urlParts.length - 1]);

                // only allow normal users access to their own record
                const currentUser = USERS_DATA.find(x => x.role === role);
                if (id !== currentUser.id && role !== Role.Admin) { return unauthorised(); }

                const user = USERS_DATA.find(x => x.id === id);
                return ok(user);
            }

            // get all users (admin only)
            if (request.url.endsWith('/users') && request.method === 'GET') {
                if (role !== Role.Admin) { return unauthorised(); }
                return ok(USERS_DATA);
            }

            if (request.url.endsWith('/menu') && request.method === 'POST') {
                return ok(FOOD_MENU_DATA);
            }

            if (request.url.endsWith('/menu/categories') && request.method === 'POST') {
                return ok(CATEGORY_DATA);
            }

            if (request.url.endsWith('/table/details') && request.method === 'POST') {
                return ok(TABLE_DATA);
            }

            if (request.url.endsWith('/table/reservations') && request.method === 'POST') {
                return ok(RESERVATIONS_DATA);
            }

            if (request.url.endsWith('/table/timeslots') && request.method === 'POST'){
                return ok(TIME_SLOTS);
            }
            if (request.url.endsWith('/admin-dashboard/mangers') && request.method === 'POST'){
                return ok(USERS_DATA.filter(x => x.role === 'Manager'));
            }

            if (request.url.endsWith('/admin-dashboard/counts') && request.method === 'POST'){
                const dashboardCount: string[] = [];
                dashboardCount['usersCount'] = USERS_DATA.filter(x => x.role === 'User').length;;
                dashboardCount['reservationCount'] = RESERVATIONS_DATA.length;
                dashboardCount['itemsCount'] = FOOD_MENU_DATA.length;
                dashboardCount['itemCategoryCount'] = CATEGORY_DATA.length;
                dashboardCount['tablesCount'] = TABLE_DATA.length;
                dashboardCount['ordersCount'] = ORDERS_DATA.length;
                return ok(dashboardCount);
            }
            
            if (request.url.endsWith('user/user-reservations') && request.method === 'POST'){
                return ok(RESERVATIONS_DATA.filter(x => x.userId === request.body.id));
            }

            if (request.url.endsWith('table/manager-reservations') && request.method === 'POST'){
                const tablesIds = new Array();
                TABLE_DATA.filter(x => x.manager === request.body.id).forEach(table => tablesIds.push(table.tableId));
                console.log(tablesIds);
                const data: Reservation[] = [];
                RESERVATIONS_DATA.forEach(x => {
                    tablesIds.forEach(y => {
                        if ( x.tableId === y) {
                            data.push(x);
                        }
                    });
                });
                return ok(data);
            }

            return next.handle(request);
        }))
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());

        // private helper functions

        function ok(body) {
            console.log(body);
            return of(new HttpResponse({ status: 200, body }));
        }

        function unauthorised() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function error(message) {
            return throwError({ status: 400, error: { message } });
        }
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
