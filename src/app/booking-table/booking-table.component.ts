import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { ReservationService } from '@/_services/reservation.service';

@Component({
  selector: 'app-booking-table',
  templateUrl: './booking-table.component.html',
  styleUrls: ['./booking-table.component.scss']
})
export class BookingTableComponent implements OnInit {
  isLinear = false;
  DateTimeGroup: FormGroup;
  infoFormGroup: FormGroup;

  fromDate;
  timeSlot;

  reservations;

  progress: 0;
  minDate = new Date();
  maxDate = new Date();
  timeSlots: string[];
  constructor(private _formBuilder: FormBuilder,private reservationService: ReservationService) {
    this.maxDate.setDate(this.minDate.getDate() + 10);
    this.reservationService.getAll().subscribe(data => {
      this.reservations = data;
    });
    this.reservationService.getTimeSlots().subscribe(data => {
      this.timeSlots = data;
    })
    console.log(this.reservations);
  }

  ngOnInit() {
    this.DateTimeGroup = this._formBuilder.group({
      fromDate : ['', Validators.required],
      timeSlot : ['', Validators.required],
      guests : ['', Validators.required]
    });
    this.infoFormGroup = this._formBuilder.group({
    });

  }

  // checking seat availabilty
  checkAvailabilty() {
    const fromdate =  new Date(this.DateTimeGroup.get('fromDate').value);
    const timeslot = this.DateTimeGroup.get('timeSlot').value;
    const guests = this.DateTimeGroup.get('guests').value;
    console.log(fromdate+'\n '+timeslot+'\n'+guests);
    //if(new Date(timeslot)){

    // console.log(fromdate.format());
    
    

   // }
  }


  updateProgress(data){
    this.progress = data;
  }

}
