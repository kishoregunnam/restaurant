import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Menu, FoodCategory, Table } from '@/_models';
import { Config } from './config';
import { Reservation } from '@/_models/reservation';


@Injectable({ providedIn: 'root' })
export class ReservationService {
    apiUrl: string;
    constructor(private http: HttpClient, conf: Config) { }

    getAll() {
        return this.http.post<Reservation[]>(`${this.apiUrl}/table/reservations`,{});
    }
    getTimeSlots() {
        return this.http.post<string[]>(`${this.apiUrl}/table/timeslots`,{});
    }
    getManagerResevations(id: number) {
        return this.http.post<Reservation[]>(`${this.apiUrl}/table/manager-reservations`, { id });
    }

    getUserResevations(id: number){
        return this.http.post<Reservation[]>(`${this.apiUrl}/user/user-reservations`, { id });
    }
}