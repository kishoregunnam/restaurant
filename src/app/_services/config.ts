import { Injectable } from '@angular/core';

@Injectable()
export class Config {
    
    apiUrl = 'http://localhost:4000';
    appTitle = 'paradise.';
    footer_date = '2018';
}
