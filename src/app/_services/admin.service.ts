import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@/_models';

import { Config } from '@/_services/config';

@Injectable({ providedIn: 'root' })
export class AdminService {
    apiUrl: string;
    constructor(private http: HttpClient, conf: Config) { 
        this.apiUrl = conf.apiUrl;
    }

    getAllCounts() {
        return this.http.post<string[]>(`${this.apiUrl}/admin-dashboard/counts`,{});
    }

    getAllMangers(){
        return this.http.post<User[]>(`${this.apiUrl}/admin-dashboard/mangers`,{});
    }
}