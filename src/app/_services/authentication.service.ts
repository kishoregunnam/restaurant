﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '@/_models';
import { Config } from '@/_services/config';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public apiUrl: string;
    public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public isuser: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient, conf: Config) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        this.apiUrl = conf.apiUrl;
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${this.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    this.loggedIn.next(true);
                    if (user.role === 'User'){
                        this.isuser.next(true);
                    } else {
                        this.isuser.next(false);
                    }
                }

                return user;
            }));
    }

    isAdmin() {
        const user: User = JSON.parse(localStorage.getItem('currentUser'));
        if (user.role === 'Admin'){
            return true;
        }
        return false;
    }

    isUser() {
        const user: User = JSON.parse(localStorage.getItem('currentUser'));
        if (user.role === 'User'){
            this.isuser.next(true);
        } else {
            this.isuser.next(true);
        }
        return this.isuser;
    }

    is_logged_in() {
        if ( localStorage.getItem('currentUser') === null){
            this.loggedIn.next(false);
        } else{
            this.loggedIn.next(true);
        }
        return this.loggedIn.asObservable();
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.loggedIn.next(false);
        this.currentUserSubject.next(null);
    }
}