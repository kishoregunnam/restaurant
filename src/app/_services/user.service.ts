﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@/_models';

import { Config } from '@/_services/config';

@Injectable({ providedIn: 'root' })
export class UserService {
    apiUrl: string;
    constructor(private http: HttpClient, conf: Config) { }

    getAll() {
        return this.http.get<User[]>(`${this.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get<User>(`${this.apiUrl}/users/${id}`);
    }
}