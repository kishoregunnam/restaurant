import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Menu, FoodCategory } from '@/_models';
import { Config } from './config';


@Injectable({ providedIn: 'root' })
export class MenuService {
    apiUrl: string;
    constructor(private http: HttpClient, conf: Config) { }

    getAll() {
        return this.http.post<Menu[]>(`${this.apiUrl}/menu`,{});
    }

    getMenuCategories(){
        return this.http.post<FoodCategory[]>(`${this.apiUrl}/menu/categories`,{});
    }
}