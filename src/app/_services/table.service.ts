import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Table } from '@/_models';
import { Config } from './config';


@Injectable({ providedIn: 'root' })
export class TableService {
    apiUrl: string;
    constructor(private http: HttpClient, conf: Config) { }

    getAll() {
        return this.http.post<Table[]>(`${this.apiUrl}/table/details`,{});
    }
}