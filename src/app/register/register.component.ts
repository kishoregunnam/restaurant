import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerGroup: FormGroup;
  constructor( private router: Router,private _formBuilder: FormBuilder ) {
  }

  ngOnInit() {
    this.registerGroup = this._formBuilder.group({
      firstName : ['', Validators.required],
      lastName : ['', Validators.required],
      username : ['', Validators.required],
      phone : ['', Validators.required],
      email : ['', Validators.required],
      password : ['', Validators.required],
      gender: ['', Validators.required]
    });
  }

}
