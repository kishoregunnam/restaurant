import { Component, OnInit } from '@angular/core';
import { MenuService } from '@/_services/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  panelOpenState = false;
  foodCategories;
  foodItems;
  constructor(private menu: MenuService) {
    menu.getMenuCategories().subscribe(data => {
      this.foodCategories = data;
    });
    menu.getAll().subscribe(data => {
      this.foodItems = data;
    });
   }

  ngOnInit() {
  }

}
