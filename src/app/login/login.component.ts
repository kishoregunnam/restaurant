import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '@/_services';
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({ 
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
 })
export class LoginComponent implements OnInit {

    returnUrl: string;
    error = '';
    loginGroup: FormGroup;
    loading: boolean;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        public snackBar: MatSnackBar,
        private _formBuilder: FormBuilder
    ) {
     }

    ngOnInit() {
        this.loginGroup = this._formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
        })
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
      this.loading = true;
          this.authenticationService.login(this.loginGroup.get('username').value, this.loginGroup.get('password').value)
            .pipe(first())
            .subscribe(
                data => {
                  if (data.role === 'Admin'){
                    this.router.navigate(['/admin']);
                  } else if (data.role === 'User'){
                    this.router.navigate(['/home']);
                  } else if (data.role === 'Manager') {
                    console.log('test');
                    this.router.navigate(['/worker']);
                  } else if (data.role === 'Chef') {
                    this.router.navigate(['/chef']);
                  } else {
                    this.router.navigate([this.returnUrl]);
                  }
                },
                error => {
                    this.snackBar.openFromComponent(LoginComponent, {
                      duration: 500,
                    });
                    this.error = error;
                    this.snackBar.open(this.error, 'close', {
                      duration: 2000
                    });
                    this.loading = false;
                });
    }
}
