export { AdminMenuComponent } from './admin-menu/admin-menu.component';
export { AdminHomeComponent } from './admin-home/admin-home.component';
export { AdminTableReservationsComponent } from './admin-table-reservations/admin-table-reservations.component';
export { AdminTablesComponent } from './admin-tables/admin-tables.component';
export { AdminUsersComponent } from './admin-users/admin-users.component';
export { AdminFeedbackComponent } from './admin-feedback/admin-feedback.component';
export { AdminSettingsComponent } from './admin-settings/admin-settings.component';
