import { Component, OnInit } from '@angular/core';
import { TableService } from '@/_services/table.service';
import { AdminService } from '@/_services/admin.service';



@Component({
  selector: 'app-admin-tables',
  templateUrl: './admin-tables.component.html',
  styleUrls: ['./admin-tables.component.scss']
})
export class AdminTablesComponent implements OnInit {
  columnsToDisplay = ['tableId', 'tableCapacity', 'minimumCapacity', 'manager', 'update', 'delete'];
  tablesData;
  managers;
  constructor(private tableService: TableService, private adminService: AdminService) {
    this.tableService.getAll().subscribe(data => {
      this.tablesData = data;
      this.tablesData.forEach(element => {
        element['editenable'] = false;
      });
    });
    this.adminService.getAllMangers().subscribe(data => {
      this.managers = data;
    });

   }

  ngOnInit() {
  }
  applyFilter(filterValue: string) {
    this.tablesData.filter = filterValue.trim().toLowerCase();
  }
  deleteTable(id?) {
    console.log(this.tablesData);
    this.tablesData = this.tablesData.filter(table => {
      return table.id !== id;
    });
    console.log(this.tablesData);
  }

  editTable(table) {
    table['editenable'] = !table['editenable'];
  }

}
