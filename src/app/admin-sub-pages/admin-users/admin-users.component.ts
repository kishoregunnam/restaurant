import { Component, OnInit } from '@angular/core';
import { UserService } from '@/_services';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {
  usersData: any;
  userColumns: string[] = ['id', 'username', 'role', 'email', 'phone'];
  constructor(private userService: UserService) {
    userService.getAll().subscribe(data => {
      this.usersData = data;
    });
   }

  ngOnInit() {
  }

}
