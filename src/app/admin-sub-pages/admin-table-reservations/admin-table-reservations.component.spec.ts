import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTableReservationsComponent } from './admin-table-reservations.component';

describe('AdminTableReservationsComponent', () => {
  let component: AdminTableReservationsComponent;
  let fixture: ComponentFixture<AdminTableReservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTableReservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTableReservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
