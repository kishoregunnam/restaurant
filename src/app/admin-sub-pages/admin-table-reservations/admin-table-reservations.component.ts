import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ReservationService } from '@/_services/reservation.service';

@Component({
  selector: 'app-admin-table-reservations',
  templateUrl: './admin-table-reservations.component.html',
  styleUrls: ['./admin-table-reservations.component.scss']
})
export class AdminTableReservationsComponent implements OnInit {
  DateTimeGroup: FormGroup;
  infoFormGroup: FormGroup;

  fromDate;
  timeSlot;

  progress: 0;
  minDate = new Date();
  maxDate = new Date();
  timeSlots: string[] = ['12:00', '12:30', '1:00'];

  reservationsData;
  reservationsColumns = ['id', 'fromDate', 'timeSlot', 'guests', 'tableId', 'email', 'phone', 'delete'];
  constructor(private _formBuilder: FormBuilder,private reservationService: ReservationService) {
    this.maxDate.setDate(this.minDate.getDate() + 10);
    
    reservationService.getAll().subscribe(data => {
      //console.log(data);
      this.reservationsData = data;
    });
  }

  ngOnInit() {
    this.DateTimeGroup = this._formBuilder.group({
      fromDate : ['', Validators.required],
      timeSlot : ['', Validators.required],
      guests : ['', Validators.required]
    });
    
    this.infoFormGroup = this._formBuilder.group({
    });

  }

  // checking seat availabilty
  checkAvailabilty() {
      console.log(this.DateTimeGroup.controls);
  }


}
