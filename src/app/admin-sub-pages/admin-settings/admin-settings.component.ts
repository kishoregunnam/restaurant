import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss']
})
export class AdminSettingsComponent implements OnInit {
  settings: FormGroup;
  changePassword: FormGroup;

  constructor(private _formBuilder: FormBuilder) {
    this.settings = this._formBuilder.group({
      siteName: ['', Validators.required]
    });

    this.changePassword = this._formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      reEnterPassword: ['', Validators.required]
    });
   }

  ngOnInit() {
  }

}
