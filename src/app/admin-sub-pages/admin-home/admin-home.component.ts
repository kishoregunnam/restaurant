import { Component, OnInit } from '@angular/core';
import { ReservationService } from '@/_services';
import { AdminService } from '@/_services/admin.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  reservationsData;
  reservationsColumns = ['id', 'fromDate', 'timeSlot', 'guests', 'tableId', 'email', 'phone', 'delete'];
  adminCounts;

  constructor(private reservationService: ReservationService, private adminService: AdminService) {
    adminService.getAllCounts().subscribe(data =>{
      this.adminCounts = data;
    });
    reservationService.getAll().subscribe(data => {
      // console.log(data);
      this.reservationsData = data;
    });
    console.log(this.adminCounts);
   }

  ngOnInit() {
  }

}
