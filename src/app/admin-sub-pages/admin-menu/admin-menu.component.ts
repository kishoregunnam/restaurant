import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Menu, FoodCategory } from '@/_models';
import { MenuService } from '@/_services/menu.service';




@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {

  categoriesColumns: string[] = ['id', 'name', 'delete', 'update'];
  foodColumns: string[] = ['id', 'name', 'category', 'price', 'delete', 'update'];
  foodMenu;
  categoriesData;
  MENU_DATA;
  CATEGORIES_DATA;
  MenuCategoriesGroup: FormGroup;
  MenuItemGroup: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _formBuilder: FormBuilder, private menu: MenuService) {
    this.menu.getAll().subscribe(data => {
      this.foodMenu = data;
      this.foodMenu.forEach(element => {
        element['editenable'] = false;
      });
      console.log(this.foodMenu);
    });
    this.menu.getMenuCategories().subscribe(data => {this.categoriesData = data; });

    this.MenuCategoriesGroup = this. _formBuilder.group({

    });
    this.MenuItemGroup = this._formBuilder.group({
        itemName : ['', Validators.required],
        itemPrice : ['', Validators.required],
        itemCategory : ['', Validators.required]
    });
  }

  ngOnInit() {
  }
  applyFilter(filterValue: string) {
    this.foodMenu.filter = filterValue.trim().toLowerCase();
  }

  deleteMenuItem(id?) {
    console.log(this.foodMenu);
    this.foodMenu = this.foodMenu.filter(item => {
      return item.id !== id;
    });
    console.log(this.foodMenu);
  }

  editMenuItem(item) {
    item['editenable'] = !item['editenable'];
  }

  addMenuItem(){
      console.log(this.MenuItemGroup.get('itemName').value);
      console.log(this.MenuItemGroup.get('itemPrice').value);
      console.log(this.MenuItemGroup.get('itemCategory').value);

  }
}
