import { Component, OnInit } from '@angular/core';
import { ReservationService } from '@/_services/reservation.service';
import { AuthenticationService } from '@/_services';

@Component({
  selector: 'app-worker-home',
  templateUrl: './worker-home.component.html',
  styleUrls: ['./worker-home.component.scss']
})
export class WorkerHomeComponent implements OnInit {
  managerReservationsData;
  reservationsColumns = ['id', 'fromDate', 'timeSlot', 'guests', 'tableId', 'email', 'phone', 'status', 'takeOrder'];
  constructor(private reservationService: ReservationService,private auth: AuthenticationService) { 
    let manager;
    auth.currentUser.subscribe(X => {
      manager = X.id;
    });
    reservationService.getManagerResevations(manager).subscribe(data=> {
      this.managerReservationsData = data;
    });
  }

  ngOnInit() {
  }

}
