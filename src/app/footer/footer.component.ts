import { Component, OnInit } from '@angular/core';
import { Config } from '@/_services/config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  date: string;
  constructor(conf: Config) {
    this.date = conf.footer_date;
   }

  ngOnInit() {
  }

}
