import { Component } from '@angular/core';

import { AuthenticationService, UserService } from '@/_services';
import { User } from '@/_models/user';
import { Observable } from 'rxjs';


@Component({ templateUrl: 'home.component.html', styleUrls: ['./home.component.scss'] })
export class HomeComponent {
  user: Observable<User>;
  userName: string;
  constructor(auth: AuthenticationService){
    this.user = auth.currentUser;
    this.user.subscribe(data => {
      this.userName = data.username;
    });
    }
}
