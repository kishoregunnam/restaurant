import { Menu } from './menu';

export class Order{
    id: number;
    userId: number;
    managerId: number;
    items: Menu[];
    totalPrice: number;
    orderedAt: string;

}