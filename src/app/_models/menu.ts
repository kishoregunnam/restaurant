export class Menu {
    id: number;
    name: string;
    description: string;
    category: string;
    price: number;
}