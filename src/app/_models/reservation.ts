import { User } from './user';

export class Reservation {
    id: number;
    userId: number;
    fromDate: string;
    guests: number;
    tableId: number;
    timeSlot: string;
    firstname: string;
    lastname: string;
    email: string;
    phone: number;
    address: string;
    note: string;
    created_at: string;
    status: string;
}
