﻿export enum Role {
    User = 'User',
    Admin = 'Admin',
    Manager = 'Manager',
    Chef  = 'Chef'
}
