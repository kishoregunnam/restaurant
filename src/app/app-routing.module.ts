import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './_guards';

import { ProfileComponent } from './profile/profile.component';
import { IndexComponent } from './index/index.component';

import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { MenuComponent } from './menu/menu.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { BookingTableComponent } from './booking-table/booking-table.component';
import { WorkerComponent } from './worker/worker.component';


import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHomeComponent, AdminMenuComponent, AdminTableReservationsComponent,
   AdminTablesComponent, AdminSettingsComponent, AdminFeedbackComponent,
   AdminUsersComponent } from '@/admin-sub-pages';

   import { TakeOrderComponent, WorkerHomeComponent } from '@/worker-sub-pages';
import { ChefComponent } from './chef/chef.component';
import { ChefHomeComponent } from './chef-sub-pages/chef-home/chef-home.component';

const routes: Routes = [
      { path : '', component : IndexComponent},
      { path : 'user', component: UserComponent, canActivate: [AuthGuard], data: {roles: ['User']} },
      { path : 'login', component: LoginComponent },
      { path : 'register', component : RegisterComponent},
      { path : 'home', component : HomeComponent, canActivate: [AuthGuard], data: {roles: ['User']}},
      { path : 'profile', component : ProfileComponent},
      { path : 'about', component : AboutComponent },
      { path : 'contact', component : ContactComponent },
      { path : 'menu', component : MenuComponent },
      { path : 'feedback', component : FeedbackComponent, canActivate: [AuthGuard] },
      { path : 'booking', component : BookingTableComponent, canActivate: [AuthGuard], data: {roles: ['User']} },
      {
        path : 'worker',
        component : WorkerComponent,
        canActivate: [AuthGuard],
        data: {roles: ['Manager']},
        children: [
          { path: '', component: WorkerHomeComponent },
          { path: 'take-order', component: TakeOrderComponent }
        ]
      },

      {
        path : 'admin',
        component : AdminDashboardComponent,
        canActivate: [AuthGuard],
        data: {roles: ['Admin'], title: ['Welcome']},
        children: [
            { path: '', component: AdminHomeComponent , data: { title: ['Welcome']}},
            { path: 'menu', component: AdminMenuComponent, data: { title: ['Food Menu']} },
            { path: 'bookings', component: AdminTableReservationsComponent},
            { path: 'tables', component: AdminTablesComponent},
            { path: 'feedback', component: AdminFeedbackComponent },
            { path: 'settings', component: AdminSettingsComponent},
            { path: 'users', component: AdminUsersComponent}
        ]
      },
      {
        path: 'chef',
        component : ChefComponent,
        canActivate: [AuthGuard],
        data: {roles: ['Chef'], title: ['Welcome']},
        children: [
          { path: '', component: ChefHomeComponent}
        ]
      }
];

// { enableTracing: true }
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
