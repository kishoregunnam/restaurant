import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';

import { fakeBackendProvider } from './_helpers';

import { CustomMaterialModule } from './core/material.module';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IndexComponent } from './index/index.component';

import { Config } from './_services';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { MenuComponent } from './menu/menu.component';
import { BookingTableComponent } from '@/booking-table/booking-table.component';
import { WorkerComponent } from './worker/worker.component';
import { AdminHomeComponent } from './admin-sub-pages/admin-home/admin-home.component';
import { AdminMenuComponent } from './admin-sub-pages/admin-menu/admin-menu.component';
import { AdminTableReservationsComponent } from './admin-sub-pages/admin-table-reservations/admin-table-reservations.component';
import { AdminTablesComponent } from './admin-sub-pages/admin-tables/admin-tables.component';
import { AdminFeedbackComponent } from './admin-sub-pages/admin-feedback/admin-feedback.component';
import { AdminSettingsComponent } from './admin-sub-pages/admin-settings/admin-settings.component';
import { AdminUsersComponent } from './admin-sub-pages/admin-users/admin-users.component';
import { GalleryComponent } from './gallery/gallery.component';
import { TakeOrderComponent } from './worker-sub-pages/take-order/take-order.component';
import { WorkerHomeComponent } from './worker-sub-pages/worker-home/worker-home.component';
import { ChefComponent } from './chef/chef.component';
import { ChefHomeComponent } from './chef-sub-pages/chef-home/chef-home.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RegisterComponent,
    ProfileComponent,
    IndexComponent,
    AdminDashboardComponent,
    AboutComponent,
    ContactComponent,
    FeedbackComponent,
    MenuComponent,
    BookingTableComponent,
    WorkerComponent,
    AdminHomeComponent,
    AdminMenuComponent,
    AdminTableReservationsComponent,
    AdminTablesComponent,
    AdminFeedbackComponent,
    AdminSettingsComponent,
    AdminUsersComponent,
    GalleryComponent,
    TakeOrderComponent,
    WorkerHomeComponent,
    ChefComponent,
    ChefHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [
    Config,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

