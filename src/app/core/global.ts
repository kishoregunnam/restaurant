import { Injectable } from '@angular/core';

@Injectable()
export class Global {

  appName = 'Paradise';

  isLoggedIn = false;
  customerName = '';
  userId: number = null;
}