import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatRadioModule,
  MatExpansionModule, MatStepperModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule,
  MatSidenavModule, MatTabsModule, MatSnackBarModule, MatProgressBarModule, MatButtonToggleModule,
  MatBadgeModule, MatChipsModule, MatDividerModule, MatPaginatorModule 
} from '@angular/material';
@NgModule({
  imports: [
  CommonModule, MatToolbarModule,
  MatButtonModule, MatCardModule,
  MatInputModule, MatDialogModule,
  MatTableModule, MatMenuModule,
  MatIconModule, MatProgressSpinnerModule,
  MatRadioModule, MatExpansionModule,
  MatStepperModule, MatDatepickerModule,
  MatNativeDateModule, MatSelectModule,
  MatSidenavModule, MatTabsModule,
  MatSnackBarModule, MatProgressBarModule,
  MatButtonToggleModule, MatBadgeModule,
  MatChipsModule, MatDividerModule,
  MatPaginatorModule
  ],
  exports: [
  CommonModule, MatToolbarModule,
  MatButtonModule, MatCardModule,
  MatInputModule, MatDialogModule,
  MatTableModule, MatMenuModule,
  MatIconModule, MatProgressSpinnerModule,
  MatRadioModule, MatExpansionModule,
  MatStepperModule, MatDatepickerModule,
  MatNativeDateModule, MatSelectModule,
  MatSidenavModule, MatTabsModule,
  MatSnackBarModule, MatProgressBarModule,
  MatButtonToggleModule, MatBadgeModule,
  MatChipsModule, MatDividerModule,
  MatPaginatorModule
   ],
})
export class CustomMaterialModule { }
