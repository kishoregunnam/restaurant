#Restaurant Management System:

##Actors of this system:##

1.User
2.Manager
3.chef
4.Administrator

###User:
The user can book a table in the restaurant using this system.

This data is collected when user books a table.

1. from date
2. Time slot
3. Number of guests

Based on this data the system checks whether tables or available or not.If available go to the next page. Otherwise, it shows an error message.

The user can give feedback to the system.

The user can view the Food Menu 
   		 The user can view the Food menu and its prices. This will help user can estimate the Bill and at the same time what items available in the restaurant.

The user can also view previous reservations.

###Manager:

Every manager assigns a few tables. Every table having reservations. the manager can get the reservations these tables.

The manager can take an order through this system.

###Administrator (admin):

Admin can manage Food menu and categories.

Manage users

Manage Reservations

Manage Tables

Manage orders.

###chef:
The chef can view the orders taken by the manager.

#How to run?

###Prerequisites :
1. npm
2. visual studio code (recommended)

##Steps to Run this project

1. Clone this repository.
2. Open with visual studio code.
3. Run these commands

   command: ```npm install```  //install node Modules
   
   command: ```ng serve```  // this command helps to execute this project.

This project starts running on 
```http://localhost:4200```
